#!/system/bin/sh

su -c 'sh /system/xbin/busybox-links.sh &> /dev/null'

# OTA platform update
mkdir -p /maps/logs
mkdir -p /maps/update/
find /maps/update/ -name '*.zip' -maxdepth 1 -exec reboot recovery ';'

sh /system/xbin/install_apks.sh > /dev/install.txt
RET=$?
cat /dev/install.txt
if [ $RET -ne 0 ]; then 
  DATE=$(date +%y%m%d_%H%M)
  echo "Moving file to /maps/logs/${DATE}_install.txt"
  mv /dev/install.txt "/maps/logs/${DATE}_install.txt"
fi

if [[ -f "/maps/startup.sh" ]]; then
  sh "/maps/startup.sh"
fi

if [[ -f "/maps/startup_once.sh" ]]; then
  sh "/maps/startup_once.sh"
  mv "/maps/startup_once.sh" "/maps/startup_was_run.sh"
fi
