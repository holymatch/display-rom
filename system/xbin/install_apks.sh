#!/system/bin/sh

# OTA Hud update

HUD="Hud.current.rom.apk"
OBD="Obd.rom.apk"

MODIFIED=0

# REBOOT=0

log () {
  echo "$(date +%F_%T): $1"
}

# Read current versions
# shopt -s nocasematch
if [[ -f "/maps/current_hud.txt" ]]; then
  while read line; do
    #echo "READ: $line"
    case "$line" in
      Hud*) HUD="$line" ;;
      HUD*) HUD="$line" ;;
      hud*) HUD="$line" ;;
    esac
  done < "/maps/current_hud.txt"
else
  log "Creating new /maps/current_hud.txt"
  echo "$HUD" > "/maps/current_hud.txt"
  MODIFIED=1
fi

if [[ -f "/maps/current_obd.txt" ]]; then
  while read line; do
    case "$line" in
      Obd*) OBD="$line" ;;
      OBD*) OBD="$line" ;;
      obd*) OBD="$line" ;;
    esac
  done < "/maps/current_obd.txt"
else
  log "Creating new /maps/current_obd.txt"
  echo "$OBD" > "/maps/current_obd.txt"
  MODIFIED=1
fi

echo "HUD=$HUD"
echo "OBD=$OBD"

# First boot of clean rom, configure defaults
if [[ -f "/system/priv-app/Hud/new_install" ]]; then
  log "New rom install, setting up..."
  rm "/system/priv-app/Hud/new_install"
  MODIFIED=1

  ln "/system/priv-app/Hud/Hud.apk" "/system/priv-app/Hud/Hud.current.apk.rom"
  rm -f "/maps/Hud.current.rom.apk"
  rm -f "/maps/Hud.legacy.rom.apk"
  cp "/system/priv-app/Hud/Hud.current.apk.rom" "/maps/Hud.current.rom.apk"
  cp "/system/priv-app/Hud/Hud.legacy.apk.rom" "/maps/Hud.legacy.rom.apk"

  if grep -q ".legacy." "/maps/current_hud.txt" ; then
    echo "Hud.legacy.rom.apk" > "/maps/current_hud.txt"
    HUD="Hud.legacy.rom.apk"
  else
    echo "Hud.current.rom.apk" > "/maps/current_hud.txt"
    HUD="Hud.current.rom.apk"
  fi
  log "HUD=$HUD"

  rm -f "/maps/Obd.rom.apk"
  cp "/system/priv-app/Obd/Obd.apk.rom" "/maps/Obd.rom.apk"
  echo "Obd.rom.apk" > "/maps/current_obd.txt"
  OBD="Obd.rom.apk"
  log "OBD=$OBD"
fi

# Check for newly downloaded apks
if [[ -f "/maps/Hud_new.apk" ]]; then
  log "Installing newly downloaded Hud"
  mv "/maps/Hud_new.apk" "/maps/Hud_installed.apk"
  echo "Hud_installed.apk" > "/maps/current_hud.txt"
  HUD="Hud_installed.apk"
  MODIFIED=1
fi
if [[ -f "/maps/Obd_new.apk" ]]; then
  log "Installing newly downloaded Obd"
  mv "/maps/Obd_new.apk" "/maps/Obd_installed.apk"
  echo "Obd_installed.apk" > "/maps/current_obd.txt"
  OBD="Obd_installed.apk"
  MODIFIED=1
fi

if [[ ! -f "/maps/$HUD" ]]; then
  log "Hud missing, set up like new install..."
  MODIFIED=1

  rm -f "/maps/Hud.current.rom.apk"
  rm -f "/maps/Hud.legacy.rom.apk"
  cp "/system/priv-app/Hud/Hud.current.apk.rom" "/maps/Hud.current.rom.apk"
  cp "/system/priv-app/Hud/Hud.legacy.apk.rom" "/maps/Hud.legacy.rom.apk"

  if grep -q ".legacy." "/maps/current_hud.txt" ; then
    echo "Hud.legacy.rom.apk" > "/maps/current_hud.txt"
    HUD="Hud.legacy.rom.apk"
  else
    echo "Hud.current.rom.apk" > "/maps/current_hud.txt"
    HUD="Hud.current.rom.apk"
  fi
  log "HUD=$HUD"
fi

if [[ ! -f "/maps/$OBD" ]]; then
  echo "Obd missing, set up like new install..."
  rm -f "/maps/Obd.rom.apk"
  cp "/system/priv-app/Obd/Obd.apk.rom" "/maps/Obd.rom.apk"
  echo "Obd.rom.apk" > "/maps/current_obd.txt"
  OBD="Obd.rom.apk"
  log "OBD=$OBD"
fi

unpack_libs () {
  # Function to extract binary libs from apk and install into system
  log "Unpacking libs for $1"
  rm -rf /maps/.lib
  mkdir /maps/.lib
  cd /maps/.lib

  unzip /system/priv-app/$1/$1.apk 'lib/armeabi-v7a/*.so'
  rm -rf /system/priv-app/$1/lib
  mkdir -p /system/priv-app/$1/lib/arm/
  mv lib/armeabi-v7a/* /system/priv-app/$1/lib/arm/
  chmod -R 755 /system/priv-app/$1
  MODIFIED=1
}

if [[ -f "/maps/$HUD" ]]; then
  if ! cmp -b "/maps/$HUD" "/system/priv-app/Hud/Hud.apk"; then
    log "Enabling new Hud: $HUD"
    rm -f /system/priv-app/Hud/Hud.apk
    ln -s "/maps/$HUD" "/system/priv-app/Hud/Hud.apk"
    unpack_libs Hud
  fi
fi

if [[ -f "/maps/$OBD" ]]; then
  if ! cmp -b "/maps/$OBD" "/system/priv-app/Obd/Obd.apk"; then
    log "Enabling new Obd"
    rm -f "/system/priv-app/Obd/Obd.apk"
    ln -s "/maps/$OBD" "/system/priv-app/Obd/Obd.apk"
    unpack_libs Obd
  fi
fi

# If any of the apk's have been updated, reboot to load cleanly.
# if [[ "$REBOOT" = "1" ]]; then 
#   reboot
# fi

exit $MODIFIED
