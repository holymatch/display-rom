#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/mmcblk2p2:6839595:337c058a17dcc5a3261fc54476cc31edd2be7e08; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/mmcblk2p1:6102311:1cae2384e87f5c7880fe2b158f915e37a899a9bd EMMC:/dev/block/mmcblk2p2 337c058a17dcc5a3261fc54476cc31edd2be7e08 6839595 1cae2384e87f5c7880fe2b158f915e37a899a9bd:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
