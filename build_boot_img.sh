DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

sudo chown -R root bootimg

pushd superrs-kitchen > /dev/null

if [[ ! -d superr_navdy ]]; then
ln -s ../ ./superr_navdy
fi

export project=navdy
export romname=navdy

romdir=$DIR
sysdir=$DIR/system
prfiles=$DIR/00_project_files
framedir=$DIR/system/framework
appdir=$DIR/system/app
privdir=$DIR/system/priv-app
usdir=$DIR/META-INF/com/google/android
logs=$DIR/00_project_files/logs

androidversion=$(grep ro.build.version.release $DIR/system/build.prop | cut -d= -f2)
api=$(grep ro.build.version.sdk $DIR/system/build.prop |  cut -d= -f2)

devicename=navdy_hud_6dl
devicechk=ro.product.device

deviceloc=$DIR/superrs-kitchen/tools/devices/navdy_hud_6dl

export 'usesudo=sudo '

source $DIR/superrs-kitchen/tools/language/english-srk

# odexstatus=Deodexed
# permtype=set_metadata

# whatimg=system
# imgfile=sys

# imgtest=$(du -hbd 0 $DIR/system | gawk '{ print $1 }')
# # tmpsize=$(cat "$deviceloc/${imgfile}imgsize")
# tmpsize=$(($imgtest + 75000000))
# syssize=$tmpsize

# sparseimg=' -s'

# make_ext4fs=$DIR/superrs-kitchen/tools/linux_tools/make_ext4fs_64
# simg2img=$DIR/superrs-kitchen/tools/linux_tools/simg2img_64
# vdexext=$DIR/superrs-kitchen/tools/linux_tools/vdexExtractor_64
# lz4=$DIR/superrs-kitchen/tools/boot/AIK/bin/linux/x86_64/lz4

AIK=$DIR/superrs-kitchen/tools/boot/AIK

aikdl() {
	sterm=$(echo "$dlinfo" | gawk '{print $2}')
	nname=$(echo "$dlinfo" | gawk '{print $1}')
	dlfile=$(wget -qO- "https://forum.xda-developers.com/showthread.php?t=2073775" | cat | grep "$sterm\.zip\|$sterm-" | cut -d'"' -f2 | grep http)
	(wget -O "$nname" "$dlfile" 2>&1) >/dev/null
	if [[ ! -f "$nname" || ! $(file "$nname" | grep "Zip\|gzip") ]]; then
		rm -rf "$nname"
	fi
}
if [[ ! -d $AIK ]]; then

    mkdir -p $(dirname $AIK)
    cd $(dirname $AIK)

    if [[ -f $DIR/aik.tar.gz ]]; then
        echo "Extracting AIK"
        tar xf $DIR/aik.tar.gz
    else
    	echo "Downloading AIK"
    	dlinfo="aik.tar.gz AIK-Linux"
    	aikdl
    	tar -xf aik.tar.gz
    	rm -rf aik.tar.gz
    fi
    mv AIK-Linux AIK
fi

chosenimg=boot
chosenimg2=boot.img
ramdir=$DIR/bootimg/ramdisk
dfprop=/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/bootimg/ramdisk/default.prop
dircur=/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/bootimg/ramdisk
fsdir='/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/system\n/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/bootimg/ramdisk\n/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/system/vendor/etc\n/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/vendor/etc\n/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/recoveryimg/ramdisk/etc'
fstmp='fstab.freescale.data
fstab.freescale.maps
fstab_sd.freescale.data
fstab_sd.freescale.maps'

fstab=/mnt/merge_g3t/navdy/superrs-kitchen/superr_navdy/bootimg/ramdisk/fstab.freescale.data

myuser=$(whoami | gawk '{ print $1 }')

#boot_repack() {
	# if [[ ! $bootext ]]; then
	# 	banner
	# 	echo "$bluet$t_boot_repack $chosenimg2 ...$normal"
	# 	echo ""
	# fi
	if [[ ! -d $prfiles/${chosenimg}_orig ]]; then
		mkdir -p $prfiles/${chosenimg}_orig
		cp $romdir/$chosenimg2 $prfiles/${chosenimg}_orig/
	fi
	${usesudo}mv $romdir/${chosenimg}img/ramdisk $AIK/
	${usesudo}mv $romdir/${chosenimg}img/split_img $AIK/
	cd $AIK
	${usesudo}./repackimg.sh
	if [[ $? = "1" ]]; then
		${usesudo}mv $AIK/ramdisk $romdir/${chosenimg}img/
		${usesudo}mv $AIK/split_img $romdir/${chosenimg}img/
		# banner
		echo "$redb$yellowt$bold$t_error$normal"
		echo "$redt$t_boot_repack_problem $chosenimg$normal"
		echo ""
		# read -p "$t_enter_continue"
		# if [[ ! $bootext ]]; then
		# 	return
		# fi
	else
		if [[ -f $romdir/${chosenimg}.img ]]; then
			${usesudo}rm $romdir/${chosenimg}.img
		fi

		${usesudo}chown -hR $myuser:$myuser image-new.img
		mv image-new.img $romdir/${chosenimg}.img
        ${usesudo}chown $(whoami) $romdir/${chosenimg}.img

		${usesudo}mv $AIK/ramdisk $romdir/${chosenimg}img/
		${usesudo}mv $AIK/split_img $romdir/${chosenimg}img/
        ${usesudo}chown -R $(whoami) $romdir/${chosenimg}img

		${usesudo}./cleanup.sh 2>&1 >> $logs/boot.log

	fi
	if [[ $islg = "lge" ]]; then
		if [[ -f $tools/open_bump.py ]]; then
			python3 $tools/open_bump.py $chosenimg.img
			mv ${chosenimg}_bumped.img $chosenimg.img
			if [[ ! $bootext ]]; then
				echo "$greent$t_boot_bump_rename $chosenimg.img$normal"
				echo ""
			fi
		fi
	fi
	if [[ ! $bootext ]]; then
		# banner
		echo "$greent$chosenimg.img$t_boot_packed_d$normal"
		echo ""
		# read -p "$t_enter_continue"
		# return
	fi
	cd $romdir
	bootext=""
	popd > /dev/null
#}
