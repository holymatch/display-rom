# check_pstate.sh

/sbin/busybox --install /system/bin
ln -s /system/bin/toolbox /system/bin/setprop

fbsplash -s /res/recovery_logo.ppm -d /dev/graphics/fb0

#mkfifo /tmp/progress
#fbsplash -s /res/recovery_logo.ppm -d /dev/graphics/fb0 -i /res/splash.ini  -f /tmp/progress &

## Wipe MAPS
#umount /maps
#mkfs.vfat -n MAPS /dev/block/mmcblk2p4
#echo -e "10" > /tmp/progress

e2fsck -p -f /dev/block/mmcblk2p6     # cache
fsck_msdos -p -f /dev/block/mmcblk2p4 # maps
e2fsck -p -f /dev/block/mmcblk2p7     # device
e2fsck -p -f /dev/block/mmcblk2p5     # system
#fsck /dev/block/mmcblk2p10   # data, going to wipe this below

mount /dev/block/mmcblk2p4   /maps
mount /dev/block/mmcblk2p6   /cache

if [ ! -d "/maps/logs/recovery" ]; then
  mkdir -p /maps/logs
  if [ -d "/cache/recovery" ]; then
    mv /cache/recovery /maps/logs/
  else
    mkdir /maps/logs/recovery
  ln -s /maps/logs/recovery /cache/recovery
  fi
fi
DATE=`date +%y%m%d_%H%M`

mkdir -p "/maps/logs/$DATE"
cp /cache/* "/maps/logs/$DATE/"

# Wipe cache (now that everything's copied off)
umount /cache
mke2fs -j -t ext4 /dev/block/mmcblk2p6
mount /dev/block/mmcblk2p6   /cache

# Wipe data
mke2fs -j -t ext4 /dev/block/mmcblk2p10

USB=$(cat /sys/class/android_usb/android0/state)

case $USB in
"CONNECTED")
    echo "Booting with USB connected, don't start recovery"
    echo "Booting with USB connected, don't start recovery" > /dev/kmsg
    break
    ;;
"CONFIGURED")
    echo "Booting with USB connected, don't start recovery"
    echo "Booting with USB connected, don't start recovery" > /dev/kmsg
    break
    ;;
*)
    echo "Booting normal mode, starting recovery"
    echo "Booting normal mode, starting recovery" > /dev/kmsg
    # setprop sys.recovery.start 1

    /system/bin/sh /install.sh 2>&1 | tee "/maps/logs/recovery/install.txt" > /dev/kmsg

    /sbin/recovery
    ;;
esac
echo -e "100" > /tmp/progress
